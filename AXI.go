//go:build linux || darwin
// +build linux darwin

package axi

import (
	"encoding/binary"
	"errors"
	"log"
	"os"
	"sync/atomic"
	"syscall"
	"unsafe"

	"github.com/alecthomas/unsafeslice"
)

const (
	AXIR int = iota
	AXIW
	AXIRW
)

// func bufftoarray(buff []byte) []uint32 {
// 	length := len(buff)
// 	array := make([]uint32, length/4)
// 	for i := 0; i < length; i += 4 {
// 		array[i/4] = (binary.BigEndian.Uint32(buff[i : i+4]))
// 	}
// 	return array
// }

// func arraytobuff(x []uint32) []byte {
// 	buf := make([]byte, len(x)*4)
// 	for i, v := range x {
// 		binary.BigEndian.PutUint32(buf[4*i:4*i+4], uint32(v))
// 	}
// 	return buf
// }

type Page struct {
	Base   int64
	Length int
}

type Register struct {
	Page   Page
	Offset int
	Mode   int
}

type RegisterController interface {
	BookPages(pages map[string]Page)
	BookRegister(page, name string, offset int, mode int)
	Read(name string) uint32
	Write(name string, value uint32)
	ReadArray(base, offset, length int64) *[]uint32
	WriteArray(addr int64, values []uint32) error
}

type AXIController struct {
	RegisterController
	memmaps map[int64][]byte
	pagemap map[string]Page
	namemap map[string]*Page
	regmap  map[string]Register
}

func (a *AXIController) BookPages(pages map[string]Page) {

	if a.regmap == nil {
		a.regmap = make(map[string]Register)
	}
	if a.namemap == nil {
		a.namemap = make(map[string]*Page)
	}
	if a.pagemap == nil {
		a.pagemap = make(map[string]Page)
	}
	if a.memmaps == nil {
		//a.memmaps = make(map[int64]mmap.MMap)
		a.memmaps = make(map[int64][]byte)

	}
	i := 0
	var err error
	var f *os.File
	for key, page := range pages {
		//Check if map exist
		if a.memmaps[page.Base] == nil {
			//log.Println("Opening mem")
			f, err = os.OpenFile("/dev/mem", os.O_RDWR|os.O_SYNC, 0666)
			if err != nil {
				log.Fatal(err)
			}
			//log.Println("mapping mem")
			a.memmaps[page.Base], err = syscall.Mmap(int(f.Fd()), page.Base, page.Length, syscall.PROT_READ|syscall.PROT_WRITE, syscall.MAP_SHARED)
			//a.memmaps[reg[0]], err = mmap.MapRegion(f, 4096, mmap.RDWR, unix.MAP_SHARED, reg[0])
			//a.memmaps[reg[0]].Lock()
			if err != nil {
				log.Fatal("MMAP:", err)
			}
			f.Close()
		}
		a.namemap[key] = &page
		i++
	}

}

func (a *AXIController) BookPage(name string, base int64, length int) {

	if a.regmap == nil {
		a.regmap = make(map[string]Register)
	}
	if a.namemap == nil {
		a.namemap = make(map[string]*Page)
	}
	if a.pagemap == nil {
		a.pagemap = make(map[string]Page)
	}
	if a.memmaps == nil {
		//a.memmaps = make(map[int64]mmap.MMap)
		a.memmaps = make(map[int64][]byte)

	}
	var err error
	var f *os.File
	//Check if map exist
	if a.memmaps[base] == nil {
		//log.Println("Opening mem")
		f, err = os.OpenFile("/dev/mem", os.O_RDWR|os.O_SYNC, 0666)
		if err != nil {
			log.Fatal(err)
		}
		//log.Println("mapping mem")
		a.memmaps[base], err = syscall.Mmap(int(f.Fd()), base, length, syscall.PROT_READ|syscall.PROT_WRITE, syscall.MAP_SHARED)
		//a.memmaps[reg[0]], err = mmap.MapRegion(f, 4096, mmap.RDWR, unix.MAP_SHARED, reg[0])
		//a.memmaps[reg[0]].Lock()
		if err != nil {
			log.Fatal("MMAP:", err)
		}
		f.Close()
	}
	page := Page{base, length}
	a.namemap[name] = &page

}

func (a *AXIController) Close() {
	for _, m := range a.memmaps {
		syscall.Munmap(m)
	}
}

func (a *AXIController) BookRegister(page, name string, offset int, mode int) {
	a.regmap[name] = Register{*a.namemap[page], offset, mode}
}

func (a *AXIController) BookRegisters(page string, registers map[string][]int64) {

	for key, desc := range registers {
		a.BookRegister(page, key, int(desc[1]), int(desc[2]))
	}
}

func (a *AXIController) Read(name string) uint32 {
	reg := a.regmap[name]
	if (reg.Mode == AXIR) || (reg.Mode == AXIRW) {
		return binary.LittleEndian.Uint32(a.memmaps[reg.Page.Base][reg.Offset*4 : reg.Offset*4+4])
	} else {
		log.Fatalf("Register %v cannot be read : %v", name, reg)
		return 0
	}
}

func (a *AXIController) Write(name string, value uint32) {
	reg := a.regmap[name]
	if (reg.Mode == AXIW) || (reg.Mode == AXIRW) {
		atomic.StoreUint32((*uint32)(unsafe.Pointer(&(a.memmaps[reg.Page.Base])[4*reg.Offset])), value)
	} else {
		log.Fatalf("Register %v cannot be written : %v", name, reg)
	}
}

func (a *AXIController) PrintMemAddress() {
	for _, reg := range a.memmaps {
		log.Printf("%p", &reg)
	}
}

func (a *AXIController) ReadArray(page string, offset, length int64) *[]uint64 {
	values := make([]uint64, length)
3333333333333333333333333	for e := offset; e < offset+length; e++ {
		values[e-offset] = binary.LittleEndian.Uint64(a.memmaps[a.namemap[page].Base][e*8 : e*8+8])
	}
	return &values
}

func (a *AXIController) ReadArray16(page string, offset, length int64) []uint16 {
	values := unsafeslice.Uint16SliceFromByteSlice(a.memmaps[a.namemap[page].Base][offset*2 : offset*2+2*length])
	return values
}

func (a *AXIController) WriteArray(page string, offset int64, values *[]uint64) error {
	if len(*values) > (len(a.memmaps[a.namemap[page].Base][8*offset:]) / 8) {
		log.Fatal("array too long for memory", (len(a.memmaps[a.namemap[page].Base][8*offset:]) / 8), len(*values))
		return errors.New("array too long for memory")
	}

	for i, v := range *values {
		pos := 8*offset + int64(i*8)
		atomic.StoreUint64((*uint64)(unsafe.Pointer(&a.memmaps[a.namemap[page].Base][pos])), v)
	}
	return nil
}

func (a *AXIController) WriteZeros(page string, offset, length int64) error {
	if int(length) > (len(a.memmaps[a.namemap[page].Base][8*offset:]) / 8) {
		log.Fatal("array too long for memory", (len(a.memmaps[a.namemap[page].Base][8*offset:]) / 8), length)
		return errors.New("array too long for memory")
	}

	for i := 0; int64(i) < length; i++ {
		pos := 8*offset + int64(i*8)
		atomic.StoreUint64((*uint64)(unsafe.Pointer(&a.memmaps[a.namemap[page].Base][pos])), 0)
	}
	return nil
}
