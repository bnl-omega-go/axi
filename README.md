# AXI Memory Controller 

The AXI Memory Controller is a convenient library to manipulate register value related to AXI-lite of AXI-Stream peripheral in a FPGA connected to the processing system. 


The Controller keep tracks of single registers (32 bits, for AXI4-lite) of memory pages by name , and allow to read and write their values . 

```go 

package main

import (
	"log"

	AXI "gitlab.cern.ch/bnl-omega-go/AXI"
)

func main() {

    // The controller
    ctrl := axi.AXIController{}
	
    //Constants of the memory page
    base := 0xA000000
    offset :=0
    value := 0

    //Book the page where single registers are lcated
    ctrl.BookPage("reg",base, offset)

    //A list of registers (base,offset in base in 32bit units,RW flag)
    registers := map[string][]int64{"REG": []int64{0xA0000000, 0, int64(axi.AXIRW)}}

    //Book specific registers, by name, in page reg
    for key, reg := range registers {
        ctrl.BookRegister("reg", key, int(reg[1]), int(reg[2]))
    }

    // write to register by name 
    if value != -1 {
        log.Printf("Writing %x to register with base %x and offset %x", *value, *base, *offset)
        ctrl.Write("REG", uint32(value))
    }

    // Read register 
    valueread := ctrl.Read("REG")
    log.Printf("Read value of register with base %x and offset %x : %x", base, offset, valueread)
    

    //AXI Stream stuff 

    //Write 2x64bit words at offset 0 in page reg
    ctrl.WriteArray("reg",0,[]int64{1,2}])

    //Read 64x64bit words at offset 0 in page reg
    readarray := ctrl.ReadArray("reg",0,64)
    
    
    return
   
}

```
