module gitlab.cern.ch/bnl-omega-go/axi

go 1.17

require (
	github.com/codehardt/mmap-go v1.0.1
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158
)

require github.com/alecthomas/unsafeslice v0.1.0 // indirect
